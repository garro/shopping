from django.template.response import TemplateResponse
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from registration.forms import RegistrationForm


def register(request, template_name="registration/register.html"):
    """
    Create a new user using django auth's built in forms
    """
    if request.method == "POST":
        registration_form = RegistrationForm(request.POST)
        if registration_form.is_valid():
            new_user = registration_form.save()
            return HttpResponseRedirect(reverse('home'))
    else:
        registration_form = RegistrationForm()
            
    return TemplateResponse(
        request,
        template_name,
        {
            'form': registration_form,
         }
    )