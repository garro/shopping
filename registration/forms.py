from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class RegistrationForm(UserCreationForm):
    """
    Form form creating a user with fields:
    'username', 'email', 'first_name', 'last_name', password1', 'password2'
    """
    email = forms.EmailField(label="Email", required=True)
    first_name = forms.CharField(max_length=100, required=True)
    last_name = forms.CharField(max_length=100, required=True)
    
    class Meta:
        model = User
        fields = ("username","email","first_name","last_name")
        
    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=True)
        user.set_password(self.cleaned_data["password1"])
        user.email = self.cleaned_data["email"]
        user.first_name = self.cleaned_data["first_name"]
        user.last_name = self.cleaned_data["last_name"]
        if commit:
            user.save()
        return user