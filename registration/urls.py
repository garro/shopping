from django.conf.urls import patterns, include, url
from registration.views import register

urlpatterns = patterns('',
    url(r'^login/$', 'django.contrib.auth.views.login', name="login"),
    url(r'^logout/$', 'django.contrib.auth.views.logout', name="logout"),
    url(r'^register/$', register, name="register")
)