# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Store'
        db.create_table(u'store_store', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('subdomain', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'store', ['Store'])


    def backwards(self, orm):
        # Deleting model 'Store'
        db.delete_table(u'store_store')


    models = {
        u'store.store': {
            'Meta': {'object_name': 'Store'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'subdomain': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        }
    }

    complete_apps = ['store']