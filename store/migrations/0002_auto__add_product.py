# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Product'
        db.create_table(u'store_product', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('price', self.gf('django.db.models.fields.DecimalField')(max_digits=6, decimal_places=2)),
            ('stock', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'store', ['Product'])

        # Adding M2M table for field stores on 'Product'
        m2m_table_name = db.shorten_name(u'store_product_stores')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('product', models.ForeignKey(orm[u'store.product'], null=False)),
            ('store', models.ForeignKey(orm[u'store.store'], null=False))
        ))
        db.create_unique(m2m_table_name, ['product_id', 'store_id'])


    def backwards(self, orm):
        # Deleting model 'Product'
        db.delete_table(u'store_product')

        # Removing M2M table for field stores on 'Product'
        db.delete_table(db.shorten_name(u'store_product_stores'))


    models = {
        u'store.product': {
            'Meta': {'object_name': 'Product'},
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'stock': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'stores': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'products'", 'symmetrical': 'False', 'to': u"orm['store.Store']"})
        },
        u'store.store': {
            'Meta': {'object_name': 'Store'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'subdomain': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        }
    }

    complete_apps = ['store']