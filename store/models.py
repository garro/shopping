from django.db import models

class Store(models.Model):
    """
    Stores corresponding to different subdomain admins can create
    """
    name = models.CharField(max_length=150)
    subdomain = models.CharField(max_length=30)
    
    class Meta:
        ordering = ['subdomain']
    
    def __unicode__(self):
        return self.name
    

class Product(models.Model):
    """
    Product model
    """
    name = models.CharField(max_length=300)
    description = models.TextField(blank=True, null=True)
    price = models.DecimalField(decimal_places=2, max_digits=6)
    stock = models.IntegerField(default=0)
    stores = models.ManyToManyField(Store, related_name="products")
    
    def __unicode__(self):
        return self.name
    