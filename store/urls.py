from django.conf.urls import patterns, include, url
from store.views import home, storefront, product

urlpatterns = patterns('',
    url(r'^$', home, name='home'),
    url(r'^store/(?P<subdomain>\w+)/', storefront, name="store"),
    url(r'^product/(?P<product_id>\w+)', product, name="product"),
)