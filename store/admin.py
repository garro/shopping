from store.models import Store, Product
from django.contrib import admin

class ProductAdmin(admin.ModelAdmin):
    """
    admins can search by product name or store information
    """
    search_fields = [
         'name',
         'stores__name',
         'stores__subdomain',
    ]

admin.site.register(Product, ProductAdmin)
admin.site.register(Store)