from django import forms
from order.models import Order, OrderProductDetails

class AddProductForm(forms.Form):
    """
    Add a product to a users order, but first check whether the quantity
    requested is in stock.
    """
    
    quantity = forms.IntegerField(required=True, min_value=1)
    
    def __init__(self, order, product, *args, **kwargs):
        self.order = order
        self.product = product
        super(AddProductForm, self).__init__(*args, **kwargs)
        
        self.fields['quantity'].initial = 1
        
    def clean(self):
        if self.cleaned_data['quantity'] > self.product.stock:
            self._errors['quantity'] = self.error_class(['Sorry, not enough units in stock.'])
        
        return self.cleaned_data
        
    def save(self):
        order = self.order
        product = self.product
        quantity = self.cleaned_data['quantity']
        
        order_product, created = OrderProductDetails.objects.get_or_create(order=order, product=product)
        order_product.quantity = quantity
        order_product.save()