from django.template.response import TemplateResponse
from store.models import Store, Product
from store.forms import AddProductForm
from order.models import Order

def home(request, template_name="home.html"):
    """
    Returns the websites front page. Not the view for sub-domains
    """
    stores = list( Store.objects.all() )
    
    return TemplateResponse(
        request,
        template_name,
        {
            'stores': stores,
        }
    )

def storefront(request, subdomain, template_name="store/storefront.html"):
    """
    Returns a storefront corresponding to an individual store
    """
    
    store = Store.objects.get(subdomain=subdomain)
    products = list( store.products.filter(stock__gt=0) )
    
    return TemplateResponse(
        request,
        template_name,
        {
            'store': store,
            'products': products
         }
    )
    
def product(request, product_id, template_name="store/product.html"):
    """
    Individual product pages. If a user trys to add a product,
    check for an existing order then validate and save the form.
    """
    user = request.user
    product = Product.objects.get(id=product_id)
    form = None
    
    if user.is_authenticated():
        order = Order.objects.get_shopping_cart(user)
        
        if request.method == "POST":
            if not order:
                order = Order.objects.create(user=user)
            form = AddProductForm(order, product, request.POST)
            if form.is_valid():
                form.save()
        else:
            form = AddProductForm(order, product)
    
    return TemplateResponse(
        request,
        template_name,
        {
            'product': product,
            'form': form
        }
    )