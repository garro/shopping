def decrease_stock(current_order):
    """
    Descrease a products stock by the corresponding 
    order_product quanity after it is checked out.
    This probably would have been better if called by a signal handler.
    """
    order_products = current_order.order_product_details.all()
    for order_product in order_products:
        product = order_product.product
        remaining_stock = product.stock - order_product.quantity
        if remaining_stock >= 0:
            product.stock = remaining_stock
            product.save()