from django.db import models
from django.contrib.auth.models import User

"""
These models were ultimately not used
"""

class Address(models.Model):
    """
    Address Model
    """
    street_address = models.CharField(max_length=300)
    country = models.CharField(max_length=50, default="United States")
    city = models.CharField(max_length=150)
    state = models.CharField(max_length=2)
    zipcode = models.CharField(max_length=15)
    
class CreditCard(models.Model):
    """
    Credit Card Model
    Forms and Validation Not Implemented
    """
    number = models.IntegerField()
    security_code = models.IntegerField()
    card_type = models.CharField(max_length=100)
    expiration_date = models.CharField(max_length=7)
    
    billing_address = models.ForeignKey(Address)

class UserProfile(models.Model):
    """
    Additional information on the user mainly used for checkout process
    """
    user = models.ForeignKey(User, unique=True)
    shipping_address = models.ForeignKey(Address, related_name="user_profile")
    credit_card = models.ForeignKey(CreditCard, related_name="user_profile")
    
    def __unicode(self):
        return self.get_full_name()
    
    def get_full_name(self):
        return self.first_name + ' ' + self.last_name