Shopping Cart Documentation
==============================

:Authors:
    Bruce Garro <brucegarro@gmail.com>
:Version: 0.0.1
:Release: 0.0.1

A simple shopping cart application written in Django. Just for fun.

Setup
==============================
Instructions are intended for a linux environment using ``virtualenv`` [virtualenv]_ and assumes use of Python 2.7.


Download Code
------------------------------
Create a folder for the code, clone the repository into it, and move into the project directory.

::

    cd /path/for/shoppingcart/code
    git clone git@bitbucket.org:garro/shopping.git
    cd shopping

VirtualEnv
------------------------------

Create a new ``virtualenv`` to house a distinct python installation to prevent contamination of the system python installation and to allow for specific version-pegging of third-party packages (including Django version 1.5.1).

::

    virtualenv /path/to/new/environment
    source /path/to/new/environment/bin/activate

With the new ``virtualenv`` active, install necessary python packages.

::

    pip install -r requirements.txt

Run Locally
------------------------------

The project defaults to a SQLite database connection and an example database is included, so from here we need only run the server.  The project includes ``runserver_plus`` from the Werkzeug [werkzeug]_ project.  Note, if you have difficulties getting the project to run, feel free to use ``runserver`` instead.

::

    python manage.py runserver_plus

From here access the server from a web browser via http://localhost:8000/

Usage
==============================

Admin account
------------------------------
The admin account on the database is:

 * Username: admin
 * Password: password

Resources
==============================

.. [virtualenv] Documentation for ``virtulenv`` can be found at http://www.virtualenv.org/en/latest/

.. [werkzeug] Documentation for ``runserver_plus`` and Werkzeug can be found at http://werkzeug.pocoo.org/