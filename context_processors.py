from order.models import Order

def core(request):
    """
    Our custom context processor. Adds user and shopping cart to context
    if user.is_authenticated()
    """
    response_data = {}

    user = request.user
    if user.is_authenticated():
        response_data['user'] = user
        
        shopping_cart = Order.objects.get_shopping_cart(user)
        response_data['shopping_cart'] = shopping_cart

    return response_data