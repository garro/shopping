from django.conf.urls import patterns, include, url
from django.contrib import admin
import settings
import store.urls
import registration.urls
import order.urls

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^', include(registration.urls)),
    url(r'^', include(store.urls)),
    url(r'^', include(order.urls)),
    url(r'^admin/', include(admin.site.urls)),
        url(r'^static/(?P<path>.*)$',
        'django.views.static.serve',
        {'document_root': settings.LOCAL_ROOT + '/static'}
    ),
)