from order.models import Order
from django.contrib import admin

class OrderAdmin(admin.ModelAdmin):
    """
    admins can search orders by email, first_name, or last name
    """
    search_fields = [
         'user__email',
         'user__first_name',
         'user__last_name',
    ]

admin.site.register(Order, OrderAdmin)