from django.conf.urls import patterns, include, url
from order.views import order_history, shopping_cart, checkout, remove_order_product

urlpatterns = patterns('',
    url(r'^order_history/$', order_history, name="order_history"),
    url(r'^shopping_cart/$', shopping_cart, name="shopping_cart"),
    url(r'^checkout/$', checkout, name="checkout"),
    url(r'^remove_order_product/(?P<product_id>\w+)', remove_order_product, name="remove_order_product"),
)