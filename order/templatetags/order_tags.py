from order.forms import AddProductForm
from django import template

register = template.Library()

@register.inclusion_tag("store/product_form.html")
def add_product_form(order, product):
    form = AddProductForm(order, product)
    return {
        'form': form
    }