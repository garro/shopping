from django.db import models
from django.contrib.auth.models import User
from store.models import Product

class OrderProductDetails(models.Model):
    """
    a through table for holding extra data on a shopping cart/order product
    """
    product = models.ForeignKey(Product,related_name="order_product_details")
    order = models.ForeignKey('Order', related_name="order_product_details")
    quantity = models.PositiveIntegerField(default=1)


class OrderManager(models.Manager):
    
    def get_shopping_cart(self, user):
        """
        Returns the users unpaid order which is the current shopping cart
        """
        orders = self.filter(user=user, is_paid=False)
        if orders.count() == 0:
            return None
        return orders[0]
    
    def get_previous_orders(self, user):
        """
        Returns only previous orders
        """
        return self.filter(user=user, is_paid=True)

class Order(models.Model):
    """
    model for current shopping cart and completed orders
    """
    user = models.ForeignKey(User, related_name="orders")
    products = models.ManyToManyField(Product, through=OrderProductDetails, related_name="order_products")
    is_paid = models.BooleanField(default=False)
    paid_at = models.DateTimeField(null=True, blank=True)
    
    objects = OrderManager()
    
    class Meta():
        ordering = ['-paid_at']
    
    def __unicode__(self):
        status = 'paid order' if self.is_paid else 'shopping cart'
        return "%s for %s %s" % (status, self.user.first_name, self.user.last_name)
    
    def is_valid_checkout(self):
        is_valid = True
        order_products = self.order_product_details.all()
        
        for order_product in order_products:
            product = order_product.product
            if order_product.quantity > product.stock:
                is_valid = False
        return is_valid
        
    