import datetime
from django.template.response import TemplateResponse
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from order.models import Order, OrderProductDetails
from store.models import Product
from utils import decrease_stock

def order_history(request, template_name="order/order_history.html"):
    """
    Show past paid orders.
    """
    user = request.user
    previous_orders = Order.objects.get_previous_orders(user=user)
    
    return TemplateResponse(
        request,
        template_name,
        {
            "previous_orders": previous_orders,
         }
    )

def shopping_cart(request, template_name="order/shopping_cart.html"):
    """
    Template Response for shopping cart. All needed data is in the context.
    """
    return TemplateResponse(
        request,
        template_name,
    )
    
def checkout(request):
    """
    Get shopping cart and save it to data.
    Decrease the product stocks afterward.
    """
    user = request.user
    if user.is_authenticated():
        current_order = Order.objects.get_shopping_cart(user=user)
        
        if current_order and current_order.is_valid_checkout():
            current_order.paid_at = datetime.datetime.now()
            current_order.is_paid = True
            current_order.save()
            decrease_stock(current_order)
    
    return HttpResponseRedirect(reverse('order_history'))

def remove_order_product(request, product_id):
    """
    Remove an order product from a shopping card.
    It's in a through table so get and delete the entry.
    """
    user = request.user
    if user.is_authenticated():
        product = Product.objects.get(id=product_id)
        shopping_cart = Order.objects.get_shopping_cart(user=user)
        order_product = OrderProductDetails.objects.get(product=product, order=shopping_cart)
        
        order_product.delete()
    
    return HttpResponseRedirect(reverse('shopping_cart'))